# -*- coding: utf-8 -*-
# Ruslan Lozhkin
# random_word.py
# Демонстрирует индексацию строк
#------------------------------------------------------------------------------#
print('''
\t\t\tПрограмма запрашивает строку у пользователя 
\t\t\tзатем печатает строку побуквенно вертикально
\t\t\tв произвольном порядке и индексирует каждую
\t\t\tбукву
      ''')
#------------------------------------------------------------------------------#
import random
words = input('\nВведите строку для дальнейшей индексации:')
n = len(words)
high = len(words)
low = -len(words)
for i in range(n):
    position = random.randrange(low, high)
    print('word[', position, ']\t', words[position])
input('\nНажмите Enter, для выхода\n')