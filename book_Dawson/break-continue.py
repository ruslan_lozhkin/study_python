#-*- coding: utf-8 -*-
# Ruslan Lozhkin 12.03.16
# break_continue.py
# show while true: if..break if...comtinue
#------------------------------------------------------------------------------#
counter = 0
while True:
    counter += 1
    if counter > 10:
        break
    if counter == 5:
        continue
    print(counter)
input('\nНажмите Enter, чтобы выйти')
